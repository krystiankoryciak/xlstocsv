﻿#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using importXlsToCsv.Lib;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;

#endregion


namespace importXlsToCsv
{
    public partial class Form1 : Form
    {
        public DatabaseConfig databaseConfig;

        public Form1()
        {
            InitializeComponent();
            databaseConfig = new DatabaseConfig();
            databaseConfig.ReadDatabaseConfXML();

            string serverName = databaseConfig.serverName;
            string dbUser = databaseConfig.dbUser;
            string dbPass = databaseConfig.dbPass;
            string dbEuriSM = databaseConfig.dbEuriSM;
            string dbSubiekt = databaseConfig.dbSubiekt;
            string ftpUri = databaseConfig.ftpUri;
            string ftpUsername = databaseConfig.ftpUsername;
            string ftpPassword = databaseConfig.ftpPassword;
            string connectionstring = databaseConfig.connectionstring;

        }
        private void btFileDirectory_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfiledialog1 = new OpenFileDialog();
            openfiledialog1.ShowDialog();
            openfiledialog1.Filter = "allfiles|*.xlsx";
            tbDirectory.Text = openfiledialog1.FileName;

        }

        private void btDownload_Click(object sender, EventArgs e)
        {

            databaseConfig.Connect();
            if (databaseConfig.connection.State != ConnectionState.Open) return;
            string path = tbDirectory.Text;
            string ConnString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";

            DataTable Data = new DataTable();

            using (OleDbConnection conn = new OleDbConnection(ConnString))
            {
                conn.Open();
                        

                OleDbCommand cmd = new OleDbCommand(@"SELECT [MC numer],[EAN],[nazwa],[link ze sklepu] FROM [Arkusz1$]", conn);

                OleDbDataAdapter adapter = new OleDbDataAdapter(cmd);
                adapter.Fill(Data);

                conn.Close();
            }

            databaseConfig.truncateTable("xls2csvImport");
            databaseConfig.truncateTable("xls2csv");

            string ConnStr = databaseConfig.connectionstring;
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConnStr))
            {

                bulkCopy.DestinationTableName = "["+ databaseConfig.dbEuriSM + "].[dbo].[xls2csvImport]";
                bulkCopy.ColumnMappings.Add("[MC numer]", "[MC numer]");
                bulkCopy.ColumnMappings.Add("[EAN]", "[EAN]");
                bulkCopy.ColumnMappings.Add("[nazwa]", "[nazwa]");
                bulkCopy.ColumnMappings.Add("[link ze sklepu]", "[link ze sklepu]");
                bulkCopy.WriteToServer(Data);

            }

            databaseConfig.updateExport1();
            databaseConfig.updateExport2();
            databaseConfig.updateExport3();
            databaseConfig.updateExport4();



           tbInfoBox.Text = ("Dane zostały dodane");

        }
       
        public static DataTable ReadTable(string connectionString, string selectQuery)
        {
            var returnValue = new DataTable();

            var conn = new SqlConnection(connectionString);

            try
            {
                conn.Open();
                var command = new SqlCommand(selectQuery, conn);

                using (var adapter = new SqlDataAdapter(command))
                {
                    adapter.Fill(returnValue);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return returnValue;
        }

        public static void WriteToFile(DataTable dataSource, string fileOutputPath, bool firstRowIsColumnHeader = false, string seperator = ";")
        {
            var sw = new StreamWriter(fileOutputPath, false);

            int icolcount = dataSource.Columns.Count;

            if (!firstRowIsColumnHeader)
            {
                for (int i = 0; i < icolcount; i++)
                {
                    sw.Write(dataSource.Columns[i]);
                    if (i < icolcount - 1)
                        sw.Write(seperator);
                }

                sw.Write(sw.NewLine);
            }

            foreach (DataRow drow in dataSource.Rows)
            {
                for (int i = 0; i < icolcount; i++)
                {
                    if (!Convert.IsDBNull(drow[i]))
                        sw.Write(drow[i].ToString());
                    if (i < icolcount - 1)
                        sw.Write(seperator);
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
            
        }

        

        private void btExport_Click(object sender, EventArgs e)
        {
            var selectQuery = "select * from xls2csv;";
            var table = ReadTable(databaseConfig.connectionstring, selectQuery);
            WriteToFile(table, @""+Application.StartupPath +"\\" + databaseConfig.uploadFilename + "", false, ";");
        }

        private void btSend_Click(object sender, EventArgs e)
        {
            try
            {
            databaseConfig.uploadFTP();
            }
            catch { }

            tbInfoBox.Text = databaseConfig.FtpGetFileTimestamp().ToString();

        }

      
    }


}
