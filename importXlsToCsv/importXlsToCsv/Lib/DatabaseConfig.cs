﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.Net;
using System.Xml;

#endregion

namespace importXlsToCsv.Lib
{

    public class DatabaseConfig
    {
        public string serverName;
        public string dbUser;
        public string dbPass;
        public string dbEuriSM;
        public string dbSubiekt;
        public string connectionstring;
        public SqlConnection connection;
        public bool Security;
        public SqlCommand dbcommand;
        public string SQLCommandText;
        public bool ServiceMode = false;

        public string FTPSubfolderid;
        public string ftpUri;
        public string ftpUsername;
        public string ftpPassword;
        public string UploadFTPpatch;
        public string uploadFilename;



        public void ReadDatabaseConfXML()
        {
            XmlDocument XMLOptions2 = new XmlDocument();
            string XMLConfigPath = (Application.StartupPath + "\\DataBaseConf.xml");
            lock (XMLOptions2)
            {
                try
                {
                    XMLOptions2.Load(XMLConfigPath);
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("serverName").Item(0).InnerText.Length > 0)
                            this.serverName = XMLOptions2.GetElementsByTagName("serverName").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("dbUser").Item(0).InnerText.Length > 0)
                            this.dbUser = XMLOptions2.GetElementsByTagName("dbUser").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("dbPass").Item(0).InnerText.Length > 0)
                            this.dbPass = XMLOptions2.GetElementsByTagName("dbPass").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("dbEuriSM").Item(0).InnerText.Length > 0)
                            this.dbEuriSM = XMLOptions2.GetElementsByTagName("dbEuriSM").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("dbSubiekt").Item(0).InnerText.Length > 0)
                            this.dbSubiekt = XMLOptions2.GetElementsByTagName("dbSubiekt").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("ftpUri").Item(0).InnerText.Length > 0)
                            this.ftpUri = XMLOptions2.GetElementsByTagName("ftpUri").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("ftpUsername").Item(0).InnerText.Length > 0)
                            this.ftpUsername = XMLOptions2.GetElementsByTagName("ftpUsername").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("ftpPassword").Item(0).InnerText.Length > 0)
                            this.ftpPassword = XMLOptions2.GetElementsByTagName("ftpPassword").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("UploadFTPpatch").Item(0).InnerText.Length > 0)
                            this.UploadFTPpatch = XMLOptions2.GetElementsByTagName("UploadFTPpatch").Item(0).InnerText;
                    }
                    catch { }
                    try
                    {
                        if (XMLOptions2.GetElementsByTagName("uploadFilename").Item(0).InnerText.Length > 0)
                            this.uploadFilename = XMLOptions2.GetElementsByTagName("uploadFilename").Item(0).InnerText;
                    }
                    catch { }
                  
                }
                catch
                { }

            }
        }


        public int Connect()
        {

            if (connection != null && connection.State == ConnectionState.Open) connection.Close();

            connection = new SqlConnection(connectionstring);
            try
            {
                if (Security) //integrated
                    connectionstring = @"Data Source=" + serverName + "; Initial Catalog=" + dbEuriSM + "; Persist Security Info= False; Integrated Security=SSPI;MultipleActiveResultSets=true";
                else


                    connectionstring = @"Data Source=" + serverName + "; Initial Catalog=" + dbEuriSM + "; Persist Security Info=True; User ID=" + dbUser + ";Password=" + dbPass + "; Connect Timeout=30; MultipleActiveResultSets=true";


                connection = new SqlConnection(connectionstring);
                connection.Open();

                return 0;
            }
            catch (Exception show)
            {
                return -1;
            }

        }
        public int truncateTable(string tableName)
        {
            return ExecuteCommand("TRUNCATE TABLE [EuriSM].[dbo].[" + tableName + "]");

        }
        public int ExecuteCommand(string command)
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
            }
            catch
            {
                MessageBox.Show("brak połączenia z bazą");
            }
            dbcommand = new SqlCommand(command, connection);
            try
            {
                dbcommand.ExecuteScalar();
                return 0;
            }
            catch (Exception Ex)
            {
                //if (command.Length < 100)
                //    MessageSend("1", Ex.Message + command); 
                //else
                //MessageSend("1", Ex.Message + command.Substring(1, 190));
                StackTrace stackTrace = new StackTrace();

                WriteErrorInfo(command + " Ret: " + Ex.Message, stackTrace.GetFrame(1).GetMethod().Name);

                // MessageBox.Show(Ex.Message + "\r\n" + command);
                // MessageBox.Show(command);
                return -1;
            };
            return -1;


        }
        public int WriteErrorInfo(string message, string Func)
        {
            if (message == "Success") return 0;
            if (!ServiceMode) return 0;

            string Cmd = "insert into Errors values ('EuriSM','" + Func + "','" + message.Replace("'", "^") + " " + System.Net.Dns.GetHostName() + "','" + DateTime.Now.ToString() + "')";
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
            }
            catch { };

            dbcommand = new SqlCommand(Cmd, connection);
            try
            {
                dbcommand.ExecuteScalar();
                return 0;
            }
            catch { return -1; }

        }

        public void writeToFile(DataTable dataSource, string fileOutputPath, bool firstRowIsColumnHeader = false, string seperator = ";")
        {
            var sw = new StreamWriter(fileOutputPath, false);

            int icolcount = dataSource.Columns.Count;

            if (!firstRowIsColumnHeader)
            {
                for (int i = 0; i < icolcount; i++)
                {
                    sw.Write(dataSource.Columns[i]);
                    if (i < icolcount - 1)
                        sw.Write(seperator);
                }

                sw.Write(sw.NewLine);
            }

            foreach (DataRow drow in dataSource.Rows)
            {
                for (int i = 0; i < icolcount; i++)
                {
                    if (!Convert.IsDBNull(drow[i]))
                        sw.Write(drow[i].ToString());
                    if (i < icolcount - 1)
                        sw.Write(seperator);
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
           
        }
        public DataTable readTable(string connectionString, string selectQuery)
        {
            var returnValue = new DataTable();

            var conn = new SqlConnection(connectionString);

            try
            {
                conn.Open();
                var command = new SqlCommand(selectQuery, conn);

                using (var adapter = new SqlDataAdapter(command))
                {
                    adapter.Fill(returnValue);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return returnValue;
        }

        public int updateExport1()
        {
            return ExecuteCommand("UPDATE e SET " +
                "e.tw_nazwa = t.tw_Nazwa," +
                "e.tw_id = t.tw_id " +
                "FROM ["+dbEuriSM+"].[dbo].[xls2csvImport] e " +
                "INNER JOIN ["+dbSubiekt+"].[dbo].[tw__Towar] t " +
                "ON e.EAN = t.tw_PodstKodKresk");
        }
        public int updateExport2()
        {
            return ExecuteCommand("UPDATE e SET " +
                "e.cena = c.tc_CenaBrutto1 " +
                "FROM ["+dbEuriSM+"].[dbo].[xls2csvImport] e " +
                "JOIN ["+dbSubiekt+"].[dbo].[tw_Cena] c " +
                "ON e.tw_id = c.tc_IdTowar");

        }
        public int updateExport3()
        {
            return ExecuteCommand("UPDATE e SET " +
                "e.stan = s.st_stan " +
                "FROM ["+dbEuriSM+"].[dbo].[xls2csvImport] e " +
                "JOIN ["+dbSubiekt+"].[dbo].[tw_Stan] s " +
                "ON e.tw_id = s.st_TowId " +
                "WHERE s.st_MagId=1 ");
        }

        public int updateExport4()
        {
            return ExecuteCommand("INSERT INTO ["+dbEuriSM+"].[dbo].[xls2csv] " +
                "([Manufacturer partnumber],[Brandname],[price],[product URL],[stock],[Productname],[EAN/ GTIN]) " +
                "SELECT [MC numer],[nazwa],[cena],[link ze sklepu],[stan],[tw_nazwa],[EAN] "+
                "FROM ["+dbEuriSM+"].[dbo].[xls2csvImport]");
        }

        #region FTP

        public int uploadFTP() //"10.184.116.69/Input"
        {
          

            FileInfo fileInf = new FileInfo(uploadFilename);

            try
            {

                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri("ftp://" + UploadFTPpatch + "/" + fileInf.Name));
                ftpRequest.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                ftpRequest.KeepAlive = false;
                ftpRequest.Timeout = 2000;
                FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                ftpResponse.Close();


            }
            catch { }

            string uri = "ftp://" + UploadFTPpatch + "/" + fileInf.Name;
            FtpWebRequest reqFTP;

            // Create FtpWebRequest object from the Uri provided
            reqFTP = (FtpWebRequest)FtpWebRequest.Create
                     (new Uri("ftp://" + UploadFTPpatch + "/" + fileInf.Name));

            // Provide the WebPermission Credintials
            reqFTP.Credentials = new NetworkCredential(ftpUsername, ftpPassword);

            // By default KeepAlive is true, where the control connection
            // is not closed after a command is executed.
            reqFTP.KeepAlive = false;

            // Specify the command to be executed.
            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;


            // Specify the data transfer type.
            reqFTP.UseBinary = true;

            // Notify the server about the size of the uploaded file
            reqFTP.ContentLength = fileInf.Length;

            // The buffer size is set to 2kb
            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            // Opens a file stream (System.IO.FileStream) to read the file
            // to be uploaded
            FileStream fs = fileInf.OpenRead();

            try
            {
                // Stream to which the file to be upload is written
                Stream strm = reqFTP.GetRequestStream();

                // Read from the file stream 2kb at a time
                contentLen = fs.Read(buff, 0, buffLength);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload
                    // Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();
                return 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                return -1;
                //MessageBox.Show(ex.Message, "Upload Error");
            }
        }


        public DateTime FtpGetFileTimestamp()
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpUri);
            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            request.Credentials =
                new NetworkCredential(ftpUsername, ftpPassword);
            try
            {
                using (FtpWebResponse response =
                    (FtpWebResponse)request.GetResponse())
                {
                    return response.LastModified;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("File unavailable"))
                    return new DateTime(3000, 1, 1);
                throw;
            }
        }

        #endregion
    }
}
