﻿namespace importXlsToCsv
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pMain = new System.Windows.Forms.Panel();
            this.tbInfoBox = new System.Windows.Forms.TextBox();
            this.btSend = new System.Windows.Forms.Button();
            this.btExport = new System.Windows.Forms.Button();
            this.tbDirectory = new System.Windows.Forms.TextBox();
            this.btWrite = new System.Windows.Forms.Button();
            this.btFileDirectory = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lbInfo = new System.Windows.Forms.Label();
            this.lbColumn = new System.Windows.Forms.Label();
            this.pMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pMain
            // 
            this.pMain.Controls.Add(this.lbColumn);
            this.pMain.Controls.Add(this.lbInfo);
            this.pMain.Controls.Add(this.tbInfoBox);
            this.pMain.Controls.Add(this.btSend);
            this.pMain.Controls.Add(this.btExport);
            this.pMain.Controls.Add(this.tbDirectory);
            this.pMain.Controls.Add(this.btWrite);
            this.pMain.Controls.Add(this.btFileDirectory);
            this.pMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pMain.Location = new System.Drawing.Point(0, 0);
            this.pMain.Name = "pMain";
            this.pMain.Size = new System.Drawing.Size(625, 280);
            this.pMain.TabIndex = 0;
            // 
            // tbInfoBox
            // 
            this.tbInfoBox.Location = new System.Drawing.Point(93, 109);
            this.tbInfoBox.Name = "tbInfoBox";
            this.tbInfoBox.Size = new System.Drawing.Size(306, 22);
            this.tbInfoBox.TabIndex = 9;
            // 
            // btSend
            // 
            this.btSend.Location = new System.Drawing.Point(470, 192);
            this.btSend.Name = "btSend";
            this.btSend.Size = new System.Drawing.Size(92, 23);
            this.btSend.TabIndex = 8;
            this.btSend.Text = "Wyślij";
            this.btSend.UseVisualStyleBackColor = true;
            this.btSend.Click += new System.EventHandler(this.btSend_Click);
            // 
            // btExport
            // 
            this.btExport.Location = new System.Drawing.Point(470, 149);
            this.btExport.Name = "btExport";
            this.btExport.Size = new System.Drawing.Size(92, 23);
            this.btExport.TabIndex = 7;
            this.btExport.Text = "Export";
            this.btExport.UseVisualStyleBackColor = true;
            this.btExport.Click += new System.EventHandler(this.btExport_Click);
            // 
            // tbDirectory
            // 
            this.tbDirectory.Location = new System.Drawing.Point(93, 71);
            this.tbDirectory.Name = "tbDirectory";
            this.tbDirectory.Size = new System.Drawing.Size(306, 22);
            this.tbDirectory.TabIndex = 6;
            // 
            // btWrite
            // 
            this.btWrite.Location = new System.Drawing.Point(470, 109);
            this.btWrite.Name = "btWrite";
            this.btWrite.Size = new System.Drawing.Size(92, 23);
            this.btWrite.TabIndex = 1;
            this.btWrite.Text = "Zapisz";
            this.btWrite.UseVisualStyleBackColor = true;
            this.btWrite.Click += new System.EventHandler(this.btDownload_Click);
            // 
            // btFileDirectory
            // 
            this.btFileDirectory.Location = new System.Drawing.Point(470, 70);
            this.btFileDirectory.Name = "btFileDirectory";
            this.btFileDirectory.Size = new System.Drawing.Size(92, 23);
            this.btFileDirectory.TabIndex = 1;
            this.btFileDirectory.Text = "Przeglądaj";
            this.btFileDirectory.UseVisualStyleBackColor = true;
            this.btFileDirectory.Click += new System.EventHandler(this.btFileDirectory_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.Location = new System.Drawing.Point(90, 38);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(163, 17);
            this.lbInfo.TabIndex = 10;
            this.lbInfo.Text = "Nazwa arkusza: Arkusz1";
            // 
            // lbColumn
            // 
            this.lbColumn.AutoSize = true;
            this.lbColumn.Location = new System.Drawing.Point(90, 9);
            this.lbColumn.Name = "lbColumn";
            this.lbColumn.Size = new System.Drawing.Size(347, 17);
            this.lbColumn.TabIndex = 11;
            this.lbColumn.Text = "Kolumny:  [MC numer], [EAN], [nazwa], [link ze sklepu]";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 280);
            this.Controls.Add(this.pMain);
            this.Name = "Form1";
            this.Text = "Form1";
            this.pMain.ResumeLayout(false);
            this.pMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pMain;
        private System.Windows.Forms.Button btFileDirectory;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox tbDirectory;
        private System.Windows.Forms.Button btWrite;
        private System.Windows.Forms.Button btExport;
        private System.Windows.Forms.Button btSend;
        private System.Windows.Forms.TextBox tbInfoBox;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Label lbColumn;
    }
}

